/**
 * Created by vitalyorlov on 10/8/16.
 */
({
    doInit : function(component, event, helper) {
        helper.getDocuments(component, '');
    },

    findImages : function(component, event, helper) {
        var name = component.find("docname").get("v.value");
        helper.getDocuments(component, name);
    },

    save : function(component, event, helper) {
        $A.util.removeClass(component.find("errorMessage"), 'visible');
        $A.util.addClass(component.find("errorMessage"), 'unvisible');
        $A.util.removeClass(component.find("downloadImage"), 'unvisible');
        $A.util.addClass(component.find("downloadImage"), 'visible');
        helper.save(component);
    },

    setSelectedFilename : function(component, event, helper) {
        var fileInput = component.find("file").getElement();
        var file = fileInput.files[0];
        component.find("selectedFilename").set("v.value", '(' + file.name + ')');
    }

})