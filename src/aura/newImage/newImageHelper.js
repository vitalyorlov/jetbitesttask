/**
 * Created by vitalyorlov on 10/8/16.
 */
({
    MAX_FILE_SIZE: 750000, /* 1 000 000 * 3/4 to account for base64 */

    save : function(component) {
        var fileInput = component.find("file").getElement();
        var file = fileInput.files[0];
        if (file == null) {
            $A.util.removeClass(component.find("downloadImage"), 'visible');
            $A.util.addClass(component.find("downloadImage"), 'unvisible');
            return;
        }

        if (file.size > this.MAX_FILE_SIZE) {
            alert('File size cannot exceed ' + this.MAX_FILE_SIZE + ' bytes.\n' +
                'Selected file size: ' + file.size);
            return;
        }

        var fr = new FileReader();

        var self = this;
        fr.onload = function() {
            var fileContents = fr.result;
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;

            fileContents = fileContents.substring(dataStart);

            self.upload(component, file, fileContents);
        };

        fr.readAsDataURL(file);
    },

    upload: function(component, file, fileContents) {
        var action = component.get("c.saveTheFile");

        action.setParams({
            fileName: file.name,
            base64Data: encodeURIComponent(fileContents),
            contentType: file.type
        });

        action.setCallback(this, function(a) {
            $A.util.removeClass(component.find("downloadImage"), 'visible');
            $A.util.addClass(component.find("downloadImage"), 'unvisible');
            var document = a.getReturnValue();
            if(document != null) {
                console.log(document);
                var documents = component.get("v.documents");
                documents.push(document);
                component.set("v.documents", documents);
            } else {
                console.log(document);
                $A.util.removeClass(component.find("errorMessage"), 'unvisible');
                $A.util.addClass(component.find("errorMessage"), 'visible');
            }
        });

        window.setTimeout(
			$A.getCallback(function() {
				$A.enqueueAction(action);
			}), 10
		);
    },

    getDocuments: function(component, name) {
        var action = component.get("c.getDocuments");
        action.setParam('name', name);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.documents", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }

})