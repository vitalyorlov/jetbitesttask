/**
 * Created by vitalyorlov on 10/12/16.
 */

@isTest
class FileControllerTest {

    @isTest
    static void saveTheFile() {
        DefaultFolder__c imageFolder = new DefaultFolder__c(ImageDir__c = 'dir', Name = 'imageFolder');
        insert imageFolder;

        Document document = FileController.saveTheFile('filename', 'das', 'image/gif');
        System.assertNotEquals(null, document);
        document = FileController.saveTheFile('filename2', 'dsad', '');
        System.assertEquals(null, document);
    }

    @isTest
    static void getDocuments() {
        DefaultFolder__c imageFolder = new DefaultFolder__c(ImageDir__c = 'dir', Name = 'imageFolder');
        insert imageFolder;

        List<Document> documents = FileController.getDocuments('');
        System.assertNotEquals(null, documents);

        Document document = FileController.saveTheFile('filename3', 'das', 'image/gif');
        documents = FileController.getDocuments(document.Name);
        System.assertNotEquals(null, documents);
    }

}