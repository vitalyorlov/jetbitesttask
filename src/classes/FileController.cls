public class FileController {

    private static String getDirectoryName() {
        DefaultFolder__c imageFolder = DefaultFolder__c.getValues('imageFolder');

        return imageFolder.ImageDir__c;
    }

    @AuraEnabled
    public static Document saveTheFile(String fileName, String base64Data, String contentType) {
        if(contentType == 'image/png' || contentType == 'image/jpeg' || contentType == 'image/gif') {

            base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');

            Folder dummyFolder = [SELECT Id FROM Folder WHERE Name = :getDirectoryName()];
            Document myDocument;
            myDocument = new Document();
            myDocument.AuthorId = UserInfo.getUserId();
            myDocument.FolderId = dummyFolder.id;
            myDocument.Body = EncodingUtil.base64Decode(base64Data);
            myDocument.ContentType = contentType;
            myDocument.Name = fileName;
            myDocument.IsPublic = true;
            insert myDocument;

            return [SELECT Id, Name, ContentType FROM Document
            WHERE FolderId =: dummyFolder.Id AND Id =: myDocument.Id];
        }
        else return null;
    }

    @AuraEnabled
    public static List<Document> getDocuments(String name) {
        Folder dummyFolder = [SELECT Id FROM Folder WHERE Name =: getDirectoryName()];
        if(name == '')
            return [SELECT Id, Name, ContentType FROM Document
                WHERE FolderId =: dummyFolder.Id and ContentType in ('image/png', 'image/jpeg', 'image/gif')];
            else {
                name = '%' + name + '%';
                return [
                        SELECT Id, Name, ContentType
                        FROM Document
                        WHERE FolderId = :dummyFolder.Id and ContentType in ('image/png', 'image/jpeg', 'image/gif')
                        and Name LIKE :name
                ];
            }
    }

}
